package pk.labs.Lab9.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;

import java.beans.*;
import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class ConsultationBeanListImpl extends java.lang.Object implements Serializable, ConsultationList, VetoableChangeListener {

    private PropertyChangeSupport pcs = new PropertyChangeSupport(this);
    private final VetoableChangeSupport vetoableChangeSupport = new VetoableChangeSupport(this);

    public List<Consultation> consultationsList;
    public int size;

    public ConsultationBeanListImpl() {
        consultationsList = new LinkedList<Consultation>();
    }

    public ConsultationBeanListImpl(List<Consultation> consultations) {
        this.consultationsList = consultations;
    }

    public ConsultationBeanListImpl(LinkedList<Consultation> consultations){
        this.consultationsList = new LinkedList<Consultation>();
        try{
            for (Consultation consultation: consultations){
                this.addConsultation(consultation);
            }
        } catch (PropertyVetoException ex){

        }
    }

    @Override
    public int getSize() {
        return this.consultationsList.size();
    }

    @Override
    public Consultation[] getConsultation() {
        return (consultationsList.toArray(new Consultation[consultationsList.size()]));
    }
    public void setConsultation(List consultations) {
        this.consultationsList = consultations;
    }

    @Override
    public Consultation getConsultation(int index) {
        return this.getConsultation()[index];
    }

    @Override
    public void addConsultation(Consultation consultation) throws PropertyVetoException {
        try{
            int oldSize = this.getSize();
            if (oldSize > 0) {
                vetoableChangeSupport.addVetoableChangeListener((VetoableChangeListener)consultation);
                vetoableChangeSupport.fireVetoableChange("consultation", consultationsList.get(getSize() - 1), consultation);
            }
            ((ConsultationBeanImpl)consultation).addVetoableChangeListener(this);
            consultationsList.add(consultation);
            pcs.firePropertyChange("consultation", oldSize, oldSize + 1);
        } catch(PropertyVetoException e) {
            throw e;
        }
    }

    @Override
    public void removePropertyChangeListener(PropertyChangeListener listener) {
        pcs.removePropertyChangeListener(listener);
    }

    @Override
    public void addPropertyChangeListener(PropertyChangeListener listener) {
        pcs.addPropertyChangeListener(listener);
    }

    public void addVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.addVetoableChangeListener(listener);
    }

    public void removeVetoableChangeListener(VetoableChangeListener listener) {
        vetoableChangeSupport.removeVetoableChangeListener(listener);
    }

    public void vetoableChange(PropertyChangeEvent event) throws PropertyVetoException {
        ConsultationBeanImpl oldCons = (ConsultationBeanImpl)event.getOldValue();
        int minutes = (Integer)event.getNewValue();
        long consStart = oldCons.getTerm().getBegin().getTime();
        long endAfterProlong = oldCons.getTerm().getEnd().getTime() + minutes * 60000;
        for (Consultation consultation : consultationsList) {
            long start = ((ConsultationBeanImpl)consultation).getTerm().getBegin().getTime();
            if((consStart < start) && (endAfterProlong >= start)) {
                throw new PropertyVetoException("duration", event);
            }
        }
    }
}
