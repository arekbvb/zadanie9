package pk.labs.Lab9;

import pk.labs.Lab9.impl.ConsultationBeanListFactoryImpl;
import pk.labs.Lab9.impl.TermBeanImpl;
import pk.labs.Lab9.impl.ConsultationBeanListImpl;
import pk.labs.Lab9.impl.ConsultationBeanImpl;
import pk.labs.Lab9.beans.*;

public class LabDescriptor {
    
    public static Class<? extends Term> termBean = TermBeanImpl.class;
    public static Class<? extends Consultation> consultationBean = ConsultationBeanImpl.class;
    public static Class<? extends ConsultationList> consultationListBean = ConsultationBeanListImpl.class;
    public static Class<? extends ConsultationListFactory> consultationListFactory = ConsultationBeanListFactoryImpl.class;
    
}
