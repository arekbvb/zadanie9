package pk.labs.Lab9.impl;

import pk.labs.Lab9.beans.Term;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

public class TermBeanImpl extends java.lang.Object implements Serializable, Term {

    public PropertyChangeSupport propertyChangeSupport;
    private Date begin;
    private int duration;

    public TermBeanImpl() {
        propertyChangeSupport = new PropertyChangeSupport(this);
        begin = new Date();
    }

    public TermBeanImpl(Date begin, int duration) {
        propertyChangeSupport = new PropertyChangeSupport(this);
        this.begin = begin;
        this.duration = duration;
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        propertyChangeSupport.removePropertyChangeListener(listener);
    }

    @Override
    public int getDuration() {
        return this.duration;
    }

    @Override
    public void setDuration(int duration) {
        if (duration>0) {
            this.duration = duration;
        }
    }

    @Override
    public Date getEnd() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(begin);
        calendar.add(Calendar.MINUTE, duration);
        return calendar.getTime();
    }

    public Date getBegin() {
        return begin;
    }

    public void setBegin(Date begin) {
        this.begin = begin;
    }
}