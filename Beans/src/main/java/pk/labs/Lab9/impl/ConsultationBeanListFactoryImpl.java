package pk.labs.Lab9.impl;

import pk.labs.Lab9.beans.Consultation;
import pk.labs.Lab9.beans.ConsultationList;
import pk.labs.Lab9.beans.ConsultationListFactory;

import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class ConsultationBeanListFactoryImpl implements ConsultationListFactory{
    @Override
    public ConsultationList create() {
        return new ConsultationBeanListImpl();
    }

    @Override
    public ConsultationList create(boolean deserialize) {
        if(deserialize){
            try{
                FileInputStream file= new FileInputStream("consultations.xml");
                BufferedInputStream buffered = new BufferedInputStream(file);
                XMLDecoder xml = new XMLDecoder(buffered);
                LinkedList<Consultation> deserializ = (LinkedList<Consultation>) xml.readObject();
                return new ConsultationBeanListImpl(deserializ);
            }
            catch(FileNotFoundException ex) {
            }
        }

        return this.create();
    }

    @Override
    public void save(ConsultationList consultationList) {
        LinkedList<Consultation> serializations = new LinkedList<Consultation>();
        for(Consultation consultation: consultationList.getConsultation()){
            serializations.add(consultation);
        }
        try {
            XMLEncoder encoder = new XMLEncoder(new BufferedOutputStream(
                    new FileOutputStream("consultations.xml")));
            encoder.writeObject(serializations);

            encoder.close();
        } catch (FileNotFoundException ex) {

        }
    }
}
